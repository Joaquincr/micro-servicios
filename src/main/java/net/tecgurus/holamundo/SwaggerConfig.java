package net.tecgurus.holamundo;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final Contact DEFAULT_CONTACT = new Contact(
            "Tec Gurus",
            "http://www.tecgurus.net",
            "contacto@tecgurus.net"
    );

    /*public static final ApiInfo DEFAULT_API_INFO = new ApiInfo(
            "API HOLA MUNDO",
            "API DE PRUEBA",
            "0.0.1",
            "url con terminos del servicio",
            DEFAULT_CONTACT,
            "Apache 2.0",
            "http://.../licencia",
            null
    );*/

    public static final ApiInfo DEFAULT_API_INFO =  new ApiInfoBuilder().title("API HOLA MUNDO")
            .description("API DE PRUEBA")
            .contact(DEFAULT_CONTACT)
            .license("Apache 2.0")
            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
            .version("1.0.0")
            .build();



    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(DEFAULT_API_INFO)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

}
