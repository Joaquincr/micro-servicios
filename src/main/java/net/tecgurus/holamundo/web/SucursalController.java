package net.tecgurus.holamundo.web;

import com.sun.net.httpserver.Headers;
import net.tecgurus.holamundo.dao.SucursalDao;
import net.tecgurus.holamundo.dto.SucursalDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@RestController
public class SucursalController {

    @Autowired
    private SucursalDao sucursalDao;

    /**
     * TODO: Sustitución de liskov
     * @return
     */
    @GetMapping("/sucursal")
    public Collection<SucursalDto> getSucursal(){
        return sucursalDao.consultar();
    }

    @GetMapping("/sucursal/{id}")
    public SucursalDto getSucursalById(@PathVariable Integer id) {
        return sucursalDao.consultarSucursal(id);
    }

    @PostMapping("/sucursal")
    public SucursalDto insertarSucursal( @Valid @ModelAttribute SucursalDto sucursalDto){
        return sucursalDao.crearSucursal(sucursalDto.getSucursalId(), sucursalDto);
    }

    /**
     * WITH RESPONSE ENTITY
     */
    @GetMapping("/sucursal/response/entity")
    public ResponseEntity<Collection<SucursalDto>> getSucursalResponseEntity(){

        Collection<SucursalDto> sucursales = sucursalDao.consultar();

        HttpHeaders headers = new HttpHeaders();
        headers.add("TECGURUS.AUTH", "AUTHENTICATED");

        //return new ResponseEntity<>(sucursales, headers, HttpStatus.I_AM_A_TEAPOT);
        return ResponseEntity.ok().headers(headers).body(sucursales);
    }



}
