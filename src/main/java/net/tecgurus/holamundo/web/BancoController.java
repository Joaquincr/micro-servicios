package net.tecgurus.holamundo.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.tecgurus.holamundo.dao.BancoDao;
import net.tecgurus.holamundo.dto.BancoDto;
import net.tecgurus.holamundo.dto.SucursalDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Api(
    value="Api para información de bancos",
    description = "Manejo de consulta de bancos"
)
@RestController
public class BancoController {

    @Autowired
    private BancoDao bancoDao;

    @ApiOperation(value= "Consulta todos los bancos existentes", response= BancoDto[].class)
    @ApiResponses(value= {
            @ApiResponse(code = 200, message = "Respuesta exitosa en consulta de bancos"),
            @ApiResponse(code = 401, message = "Acceso no autorizado al recurso"),
            @ApiResponse(code = 404, message = "Bancos no encontrados")
    })
    @GetMapping("/bancos")
    public Collection<BancoDto> getBancos(){
        return bancoDao.consultarBancos();
    }

    @GetMapping("/hateoas/bancos")
    public Resources<BancoDto> getBancosH() {

        Collection<BancoDto> bancos = bancoDao.consultarBancos();

        for( BancoDto b : bancos ) {
            b.getLinks().clear();

            Link selfLink = linkTo(methodOn(BancoController.class).getBancoById(b.getIdBanco()))
                    .withSelfRel();
            b.add(selfLink);

            if (b.getSucursales().size() > 0) {
                for (SucursalDto s : b.getSucursales()) {
                    s.getLinks().clear();

                    Link selfLinkSucursal =
                            linkTo(methodOn(SucursalController.class).getSucursalById(s.getSucursalId()))
                            .withSelfRel();
                    s.add(selfLinkSucursal);

                }
            }
        }

        return new Resources<>(bancos, linkTo(methodOn(BancoController.class).getBancosH()).withSelfRel());
    }

    @GetMapping("/bancos/{id}")
    public ResponseEntity<Resource<BancoDto>> getBancoById(@PathVariable Integer id) {
        BancoDto bancoDto = bancoDao.consultarBanco(id);

        if(bancoDto == null) {
            return  ResponseEntity.notFound().build();
        }

        bancoDto.getLinks().clear();
        Link selfLink = linkTo(methodOn(BancoController.class).getBancoById(bancoDto.getIdBanco()))
                .withSelfRel();

        Link allBancos = linkTo(methodOn(BancoController.class).getBancosH()).withRel("bancos");
        bancoDto.add(selfLink);
        bancoDto.add(allBancos);
        Resource<BancoDto> response =  new Resource<>(bancoDto, linkTo(methodOn(BancoController.class).getBancoById(id)).withSelfRel());
        return ResponseEntity.ok(response);
    }
}
