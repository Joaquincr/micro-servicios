package net.tecgurus.holamundo.web.client;

import lombok.extern.java.Log;
import net.tecgurus.holamundo.dao.SucursalDao;
import net.tecgurus.holamundo.dto.BancoDto;
import net.tecgurus.holamundo.dto.SucursalDto;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
public @Log class RestTemplateController {

    @GetMapping("/cliente/banco/{id}")
    public ResponseEntity<BancoDto> getBanco(@PathVariable Integer id){
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers  = new HttpHeaders();

        ResponseEntity<BancoDto> response = null;

        Map<String, Integer> params = new HashMap<>();
        params.put("id", id);

        String endpoint = "http://localhost:8080/bancos/{id}";

        log.info("client, endpoint: " + endpoint.replace("{id}", id.toString()));

        try {
            response = rest.exchange(
                    endpoint, //URI
                    HttpMethod.GET, //Metodo HTTP
                    new HttpEntity<>(headers), //Cabecesar
                    BancoDto.class, //Tipo de respuesta
                    params //parametros de path
            );

        }catch(Exception e) {
            log.warning(e.getMessage());
        }

        if(response == null) {
            return ResponseEntity.notFound().build();
        }

        return response;
    }

    @GetMapping("/cliente/sucursal/{id}")
    public ResponseEntity<SucursalDto> getSucursal(@PathVariable Integer id){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        ResponseEntity<SucursalDto> response = null;

        Map<String, Integer> params = new HashMap<>();
        params.put("id", id);

        String endpoint = "http://localhost:8100/sucursal/{id}";

        log.info("client, endpoint: " + endpoint.replace("{id}", id.toString()));

        try {
            response = restTemplate.exchange(
                    endpoint,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    SucursalDto.class,
                    params
            );
        } catch(Exception e){
            log.warning(e.getMessage());
        }

        if(response == null) {
            return ResponseEntity.notFound().build();
        }

        return response;
    }

    /*@GetMapping("/cliente/posdata")
    public String getSucursal(){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        ResponseEntity<SucursalDto> response = null;


        String endpoint = "http://posdata-246302.appspot.com/user/5d281da711286a03d4287a9d";

        log.info("client, endpoint: ");

        try {
            response = restTemplate.exchange(
                    endpoint,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    SucursalDto.class
            );
        } catch(Exception e){
            log.warning(e.getMessage());
        }


        return response.toString();
    }*/
}
