package net.tecgurus.holamundo.web;

import net.tecgurus.holamundo.dao.SucursalDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class HolaMundoController {

    @Autowired
    SucursalDao sucursalDao;

    @GetMapping("/saludo")
    public String saludarGet(){
        return "Hola mundo GET";
    }

    @PostMapping("/saludo")
    public String saludarPost(){
        return "saludo POST";
    }

    @PutMapping("/saludo")
    public String saludarPut(){
        return "saludo PUT";
    }

    @DeleteMapping("/saludo")
    public String saludarDelete() {
        return "saludo DELETE";
    }
}
