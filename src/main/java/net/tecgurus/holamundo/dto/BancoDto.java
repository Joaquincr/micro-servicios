package net.tecgurus.holamundo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Entidad que representa la información de un banco")
public @Data class BancoDto extends ResourceSupport {

    @ApiModelProperty(notes="El id del banco")
    private Integer idBanco;
    @ApiModelProperty(notes="Nombre del banco")
    private String nombre;

    @ApiModelProperty(notes="Sucursales que contienen los bancos")
    private Collection<SucursalDto> sucursales;
}
