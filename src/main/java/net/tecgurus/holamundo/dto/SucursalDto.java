package net.tecgurus.holamundo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public @Data class SucursalDto extends ResourceSupport {

    @Min(5)
    @Max(10)
    @NotNull(message="Sucursal id no puede ser nulo")
    private Integer sucursalId;

    @NotBlank(message = "El no mbre no puede estar en blanco")
    private String nombre;
}
