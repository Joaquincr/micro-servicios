package net.tecgurus.holamundo.dao;

import net.tecgurus.holamundo.dto.BancoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class BancoDao {

    @Autowired
    private SucursalDao sucursalDao;

    private static Map<Integer, BancoDto> mapaBancos;


    //TODO: @PostConstruct hace que el metodo se ejecute despues de crear el bean en spring
    @PostConstruct
    private void init() {
        mapaBancos = new HashMap<>();
        mapaBancos.put(1, new BancoDto(1, "Banamex", sucursalDao.consultar()));
        mapaBancos.put(2, new BancoDto(2, "Santander", sucursalDao.consultar()));
        mapaBancos.put(3, new BancoDto(3, "BBVA", sucursalDao.consultar()));
    }

    public Collection<BancoDto> consultarBancos() {
        return mapaBancos.values();
    }

    public BancoDto consultarBanco(Integer id){
        return mapaBancos.get(id);
    }

    public BancoDto createBanco(Integer key, BancoDto bancoDto) {
        mapaBancos.put(key, bancoDto);
        return bancoDto;
    }

    public void borrarBanco(Integer id){
        mapaBancos.remove(id);
    }

 }
