package net.tecgurus.holamundo.dao;

import net.tecgurus.holamundo.dto.SucursalDto;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class SucursalDao {
    private static Map<Integer, SucursalDto> mapaSucursales;

    static {
        mapaSucursales = new HashMap<>();
        mapaSucursales.put(1, new SucursalDto(1, "sucursal uno"));
        mapaSucursales.put(2, new SucursalDto(2, "sucursal dos"));
        mapaSucursales.put(3, new SucursalDto(3, "sucursal tres"));
        mapaSucursales.put(4, new SucursalDto(4, "sucursal cuatro"));
        mapaSucursales.put(5, new SucursalDto(5, "sucursal cinco"));
        mapaSucursales.put(6, new SucursalDto(6, "sucursal seis"));
    }

    public Collection<SucursalDto> consultar() {
        return mapaSucursales.values();
    }

    public SucursalDto consultarSucursal(Integer id){
        return mapaSucursales.get(id);
    }

    public  SucursalDto crearSucursal(Integer key, SucursalDto sucursalDto){
        mapaSucursales.put(key, sucursalDto);
        return mapaSucursales.get(key);
    }

    public SucursalDto borrarSucursal(Integer key) {
        return mapaSucursales.remove(key);
    }
}
